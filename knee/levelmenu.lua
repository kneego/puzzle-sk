--
-- Corona Level Menu widget
-- Pavel Koci (@pavelkoci) (KneeGo (@KneeGoApps))
--
-- https://bitbucket.org/pavelkoci/corona-levelmenu/
--

local native = require( "native" )
local widget = require( "widget" )
local display = require( "display" )

local M = {}

local currentPage = 0
local pageCount
local scrollView -- main container
local levelButtonEventListener -- custom touch handler
local lastX -- last positin of the touch event
local lastScrollX, lastScrollY -- last position of the scroll view

local screenW = display.contentWidth
local screenH = display.contentHeight

local halfW = display.contentWidth * 0.5
local halfH = display.contentHeight * 0.5

local function scrollViewListener( event )
	if event.phase == 'ended' and event.target and event.target.getContentPosition then 
		local positionX, positionY = event.target:getContentPosition()

		if (currentPage * screenW) + positionX < -50 and currentPage < pageCount - 1 then
			-- scroll to next page
			currentPage = currentPage + 1
			event.target:scrollToPosition( { x = -(currentPage * screenW) } )

			return true
		elseif (currentPage * screenW) + positionX > 50 and currentPage > 0 then
			-- scroll to previous page
			currentPage = currentPage - 1
			event.target:scrollToPosition( { x = -(currentPage * screenW) } )

			return true
		else
			-- put it back
			event.target:scrollToPosition( { x = -(currentPage * screenW) } )

			return true
		end
	end
end

local function touchHandler( event )
	if event.phase == 'began' then
		-- store position of event and scroll view
		lastEventX = event.x
		lastScrollX, lastScrollY = scrollView:getContentPosition() 
		return true
	elseif event.phase == 'moved' then
		-- move scroll view content
		scrollView:scrollToPosition( { x = lastScrollX + (event.x - lastEventX), time = 0 } )
		return true
	elseif event.phase == 'ended' and lastEventX ~=nil and math.abs(lastEventX - event.x) < 15 then
		-- if content is not moved
		if levelButtonEventListener then
			-- run custom handler if defined
			return levelButtonEventListener( event )
		end
	end
end

local function addToView( options )
	local customOptions = options or {}

	-- nothing to do...
	if not customOptions.parent or not customOptions.object then return end

	local opt = {}

	opt.parent = customOptions.parent
	opt.object = customOptions.object

	opt.parent = customOptions.parent or nil
	opt.page = customOptions.page or 1

	-- position
	opt.x = customOptions.x or halfW
	opt.y = customOptions.y or halfH

	local _x = ((opt.page - 1) * screenW) + opt.x
	local _y = opt.y

	opt.object.x = _x
	opt.object.y = _y

	opt.parent:insert( opt.object )
end

function M.createMenu( options )
	local customOptions = options or {}

	local opt = {}

	local simpleLayout

	if customOptions.levels and #customOptions.levels then
		-- levels is array of level specification (locked / unlocked, name, stars, ...)
		simpleLayout = false
		opt.levels = customOptions.levels
		opt.levelCount = #customOptions.levels
	elseif customOptions.levelCount then
		-- simple layout
		simpleLayout = true
		opt.levelCount = customOptions.levelCount or 24
		opt.levelName = customOptions.levelName or ''
	else
		-- nothing to do...
		error( 'Levels or levelCount (for simple layout) must be defined.' )
	end

	opt.levelNameOffset = customOptions.levelNameOffset or 0

	-- layout specification - append level number after level name
	if customOptions.appendLevelNumber ~= nil then opt.appendLevelNumber = customOptions.appendLevelNumber else opt.appendLevelNumber = true end
	if customOptions.drawLevelNameOnLocked ~= nil then opt.drawLevelNameOnLocked = customOptions.drawLevelNameOnLocked else opt.drawLevelNameOnLocked = false end

	-- which level is last unlocked?
	opt.unlockedTo = customOptions.unlockedTo or 1

	-- background images
	opt.imageUnlocked = customOptions.imageUnlocked
	opt.imageLocked = customOptions.imageLocked
	opt.imageStar = customOptions.imageStar
	opt.imageStarOff = customOptions.imageStarOff
	opt.imageStarWidth = customOptions.imageStarWidth
	opt.imageStarHeight = customOptions.imageStarHeight
	opt.imageStarCount = customOptions.imageStarCount or 3

	-- font and color specification
	opt.font = customOptions.font or native.systemFont
	opt.fontSize = customOptions.fontSize or 13
	opt.fontColor = customOptions.fontColor

	-- size of the main container
	opt.width = customOptions.width or screenW
	opt.height = customOptions.height or screenH

	-- position of the main container
	opt.left = customOptions.left or 0
	opt.top = customOptions.top or 0

	-- how many rows and columns will be on one page
	opt.rows = customOptions.rows or 4
	opt.columns = customOptions.columns or 3

	-- item size
	opt.itemWidth = customOptions.itemWidth or 80
	opt.itemHeight = customOptions.itemHeight or 80

	-- pading
	opt.paddingTop = customOptions.padding or 0
	opt.paddingBottom = customOptions.padding or 0
	opt.paddingLeft = customOptions.padding or 0
	opt.paddingRight = customOptions.padding or 0

	if customOptions.paddingTop ~= nil then opt.paddingTop = customOptions.paddingTop end
	if customOptions.paddingBottom ~= nil then opt.paddingBottom = customOptions.paddingBottom end
	if customOptions.paddingLeft ~= nil then opt.paddingLeft = customOptions.paddingLeft end
	if customOptions.paddingRight ~= nil then opt.paddingRight = customOptions.paddingRight end

	opt.starSpace = customOptions.starSpace or 8
	opt.starOffset = customOptions.starOffset or 50
	opt.starOnLocked = customOptions.starOnLocked

	-- event handler
	if customOptions.eventListener and type(customOptions.eventListener) == 'function' then
		levelButtonEventListener = customOptions.eventListener
	end

	-- main container
	scrollView = widget.newScrollView( {
		hideBackground = true, verticalScrollDisabled = true,
		left = opt.left, top = opt.top, width = opt.width, height = opt.height,
		listener = scrollViewListener
	} )

	-- calculate offsets
	local contentWidth = opt.width - opt.paddingLeft - opt.paddingRight
	local boxWidth = contentWidth / opt.columns
	local leftOffset = boxWidth / 2

	local contentHeight = opt.height - opt.paddingTop - opt.paddingBottom
	local boxHeight = contentHeight / opt.rows
	local topOffset = boxHeight / 2

	local levelName

	local row = 0
	local column = 0
	local page = 1
	local pageUsed = false

	for levelIndex = 1, opt.levelCount do
		x = opt.paddingLeft + leftOffset + column * boxWidth
		y = opt.paddingTop + topOffset + row * boxHeight

		local itemGroup = display.newGroup( )

		-- box background
		local square = display.newRect( 0, 0, opt.itemWidth, opt.itemHeight )
		square.levelNumber = levelIndex

		if customOptions.drawSquare then 
			square:setFillColor( 0, 0, 0 )
		else
			square.alpha = 0
			square.isHitTestable = true
		end

		itemGroup:insert( square )
		
		-- level name and stars
		if simpleLayout then
			levelName = opt.levelName .. levelIndex
		else
			levelName = opt.levels[levelIndex].name

			-- if name is not defined
			if not levelName then levelName = 'LEVEL' end

			if opt.appendLevelNumber then
				-- append level number
				levelName = levelName .. ' ' .. levelIndex
			end

			-- add stars
			if levelIndex <= opt.unlockedTo and opt.imageStar then
				local stars = opt.levels[levelIndex].stars or 0

				local starGroup = display.newGroup( )
				local space

				for i = 1, opt.imageStarCount do
					local _starImage

					if i <= stars then
						-- unlocked star
						if opt.imageStarWidth and opt.imageStarHeight then
							_starImage = display.newImageRect( opt.imageStar, opt.imageStarWidth, opt.imageStarHeight )
						else
							_starImage = display.newImage( opt.imageStar )
						end
					else
						-- locked star
						if opt.imageStarOff then
							if opt.imageStarWidth and opt.imageStarHeight then
								_starImage = display.newImageRect( opt.imageStarOff, opt.imageStarWidth, opt.imageStarHeight )
							else
								_starImage = display.newImage( opt.imageStarOff )
							end
						end
					end

					if _starImage then
						local _offset
						if opt.imageStarOff then
							_offset = (_starImage.width * opt.imageStarCount + opt.starSpace * (opt.imageStarCount - 1)) / 2
						else
							_offset = (_starImage.width * stars + opt.starSpace * (stars - 1)) / 2
						end

						_starImage.anchorX = 0
						_starImage.x = (i - 1) * (_starImage.width + opt.starSpace) - _offset
						
						_starImage.y = opt.starOffset

						starGroup:insert( _starImage )
					end
				end

				itemGroup:insert(starGroup)
			end
		end

		square.levelName = levelName

		local levelNameText
		local levelBackgroundImage

		if levelIndex <= opt.unlockedTo then
			-- this level is unlocked
			if opt.imageUnlocked then
				levelBackgroundImage = display.newImage( opt.imageUnlocked )
				levelBackgroundImage.width = opt.itemWidth
				levelBackgroundImage.height = opt.itemHeight
			end

			levelNameText = display.newText({ text = levelName, font = opt.font, fontSize = opt.fontSize })
			if opt.fontColor then levelNameText:setFillColor( unpack( opt.fontColor ) ) end

			square:addEventListener( 'touch', touchHandler )
		else
			-- locked level
			if opt.imageLocked then
				levelBackgroundImage = display.newImage( opt.imageLocked )
				levelBackgroundImage.width = opt.itemWidth
				levelBackgroundImage.height = opt.itemHeight
			end

			if opt.drawLevelNameOnLocked then
				levelNameText = display.newText({ text = levelName, font = opt.font, fontSize = opt.fontSize })
				if opt.fontColor then levelNameText:setFillColor( unpack( opt.fontColor ) ) end
			end
		end

		if levelBackgroundImage then itemGroup:insert( 1, levelBackgroundImage ) end

		if levelNameText then
			levelNameText.y = opt.levelNameOffset

			itemGroup:insert( levelNameText )
		end

		-- add text to container
		addToView({
			parent = scrollView, object = itemGroup, page = page, x = x, y = y
		})

		-- this page was used
		pageUsed = true

		-- update row and column
		if column < opt.columns - 1 then column = column + 1 else 
			column = 0

			if row < opt.rows - 1 then row = row + 1 else 
				row = 0
				page = page + 1
				pageUsed = false
			end
		end
	end

	-- remove page if it was not used 
	if not pageUsed then page = page - 1 end

	scrollView:setScrollWidth( opt.width * page)

	pageCount = page

	return scrollView
end

return M