local _M = {}

_M.category_label = {
	'hrady',
	'zvierata',
	'kvety',
	'priroda',
	'jaskyne',
}

_M.category_time_bonus = {
	20,  -- 3x3
	50,  -- 4x4
	90,  -- 5x5
	180,  -- 6x6
	270,  -- 7x7
}

_M.category_preview = {
	'images/preview/hrady.jpg',
	'images/preview/zvierata.jpg',
	'images/preview/kvety.jpg',
	'images/preview/priroda.jpg',
	'images/preview/jaskyne.jpg',
}

_M.puzzle_images = {
	hrady = {
		'beckov.jpg',
		'bojnicky-zamok.jpg',
		'bratislavsky-hrad.jpg',
		'cachticky-hrad.jpg',
		'devin.jpg',
		'lupca.jpg',
		'oravsky-hrad.jpg',
		'spissky-hrad.jpg',
		'strecno.jpg',
		'trenciansky-hrad.jpg',
		'devin-zrucanina.jpg',
		'smolenicky-zamok.jpg',
	},
	zvierata = {
		'datel-cierny.jpg',
		'diviak.jpg',
		'jelen.jpg',
		'kamzik.jpg',
		'medved-hnedy.jpg',
		'liska-hrdzava.jpg',
		'orol-skalny.jpg',
		'rys-ostrovid.jpg',
		'svist.jpg',
		'medved-hnedy-rieka.jpg',
	},
	kvety = {
		'astra-spisska.jpg',
		'chryzantema.jpg',
		'plesnivec.jpg',
		'pupava.jpg',
		'sedmokraska.jpg',
		'snezienka.jpg',
		'straconozka-tatranska.jpg',
	},
	priroda = {
		'banska-stiavnica.jpg',
		'kralova-hola.jpg',
		'liptovska-mara.jpg',
		'lomnicky-stit.jpg',
		'nizke-tatry.jpg',
		'popradske-pleso.jpg',
		'skalnate-pleso.jpg',
		'vlkolinec.jpg',
		'lomnicky-stit-lanovka.jpg',
	},
	jaskyne = {
		'belianska-jaskyna.jpg',
		'demanovska-jaskyna-slobody.jpg',
		'dobsinska-ladova-jaskyna.jpg',
		'domica.jpg',
		'driny.jpg',
		'demanovska-ladova-jaskyna.jpg',
		'gombasecka-jaskyna.jpg',
		'jasovska-jaskyna.jpg',
		'vazecka-jaskyna.jpg',
		'jaskyna-driny.jpg',
	},
}

_M.admob_id = {
	['iPhone OS'] = 'ca-app-pub-1210274935500874/3426624342',
	['Android'] = 'ca-app-pub-1210274935500874/7996424745'
}

_M.board_id = {
	['iPhone OS'] = {
		'puzzle_sk_3x3_v2',
		'puzzle_sk_4x4_v2',
		'puzzle_sk_5x5_v2',
		'puzzle_sk_6x6_v2',
		'puzzle_sk_7x7_v2',
	},
	['Android'] = {
		'CgkIpJ2vle8REAIQBA', -- child
		'CgkIpJ2vle8REAIQAw', -- easy
		'CgkIpJ2vle8REAIQAg', -- standard
		'CgkIpJ2vle8REAIQAQ', -- medium
		'CgkIpJ2vle8REAIQAA', -- hard
	}
}

_M.achievements_play = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIpJ2vle8REAIQFg', 'CgkIpJ2vle8REAIQFw', 'CgkIpJ2vle8REAIQGA', 'CgkIpJ2vle8REAIQGQ'},
		{'CgkIpJ2vle8REAIQEg', 'CgkIpJ2vle8REAIQEw', 'CgkIpJ2vle8REAIQFA', 'CgkIpJ2vle8REAIQFQ'},
		{'CgkIpJ2vle8REAIQDg', 'CgkIpJ2vle8REAIQDw', 'CgkIpJ2vle8REAIQEA', 'CgkIpJ2vle8REAIQEQ'},
		{'CgkIpJ2vle8REAIQCQ', 'CgkIpJ2vle8REAIQCw', 'CgkIpJ2vle8REAIQDA', 'CgkIpJ2vle8REAIQDQ'},
		{'CgkIpJ2vle8REAIQBQ', 'CgkIpJ2vle8REAIQBg', 'CgkIpJ2vle8REAIQBw', 'CgkIpJ2vle8REAIQCA'},
	}
}

_M.achievements_play_three_stars = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIpJ2vle8REAIQKw', 'CgkIpJ2vle8REAIQLA', 'CgkIpJ2vle8REAIQLQ', 'CgkIpJ2vle8REAIQLg'},
		{'CgkIpJ2vle8REAIQJw', 'CgkIpJ2vle8REAIQKA', 'CgkIpJ2vle8REAIQKQ', 'CgkIpJ2vle8REAIQKg'},
		{'CgkIpJ2vle8REAIQIw', 'CgkIpJ2vle8REAIQJA', 'CgkIpJ2vle8REAIQJQ', 'CgkIpJ2vle8REAIQJg'},
		{'CgkIpJ2vle8REAIQHw', 'CgkIpJ2vle8REAIQIA', 'CgkIpJ2vle8REAIQIQ', 'CgkIpJ2vle8REAIQIg'},
		{'CgkIpJ2vle8REAIQGw', 'CgkIpJ2vle8REAIQHA', 'CgkIpJ2vle8REAIQHQ', 'CgkIpJ2vle8REAIQHg'},
	}
}

_M.achievements_five_minutes = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIpJ2vle8REAIQPw', 'CgkIpJ2vle8REAIQQA', 'CgkIpJ2vle8REAIQQQ', 'CgkIpJ2vle8REAIQQg'},
		{'CgkIpJ2vle8REAIQOw', 'CgkIpJ2vle8REAIQPA', 'CgkIpJ2vle8REAIQPQ', 'CgkIpJ2vle8REAIQPg'},
		{'CgkIpJ2vle8REAIQNw', 'CgkIpJ2vle8REAIQOA', 'CgkIpJ2vle8REAIQOQ', 'CgkIpJ2vle8REAIQOg'},
		{'CgkIpJ2vle8REAIQMw', 'CgkIpJ2vle8REAIQNA', 'CgkIpJ2vle8REAIQNQ', 'CgkIpJ2vle8REAIQNg'},
		{'CgkIpJ2vle8REAIQLw', 'CgkIpJ2vle8REAIQMA', 'CgkIpJ2vle8REAIQMQ', 'CgkIpJ2vle8REAIQMg'},
	}
}

_M.achievements_one_minute = {
	['iPhone OS'] = {
	},
	['Android'] = {
		{'CgkIpJ2vle8REAIQUw', 'CgkIpJ2vle8REAIQVA', 'CgkIpJ2vle8REAIQVQ', 'CgkIpJ2vle8REAIQVg'},
		{'CgkIpJ2vle8REAIQTw', 'CgkIpJ2vle8REAIQUA', 'CgkIpJ2vle8REAIQUQ', 'CgkIpJ2vle8REAIQUg'},
		{'CgkIpJ2vle8REAIQSw', 'CgkIpJ2vle8REAIQTA', 'CgkIpJ2vle8REAIQTQ', 'CgkIpJ2vle8REAIQTg'},
		{'CgkIpJ2vle8REAIQRw', 'CgkIpJ2vle8REAIQSA', 'CgkIpJ2vle8REAIQSQ', 'CgkIpJ2vle8REAIQSg'},
		{'CgkIpJ2vle8REAIQQw', 'CgkIpJ2vle8REAIQRA', 'CgkIpJ2vle8REAIQRQ', 'CgkIpJ2vle8REAIQRg'},
	}
}

return _M