local values = {
	-- menu
	game_name_1 = 'Slovenské',
	game_name_2 = 'Puzzle',

	classic_play = 'KLASICKÁ HRA',
	time_challenge = 'ČASOVÁ VÝZVA',
	random_puzzle = 'NÁHODNÉ PUZZLE',
	leaderboard = 'NAJLEPŠÍ HRÁČI',
	settings = 'NASTAVENIA',

	-- settings
	settings_head = 'NASTAVENIA',
	sounds = 'ZVUKY',
	music = 'HUDBA',
	vibrations = 'VIBRÁCIE',

	-- select category
	select_category = 'VOĽBA KATEGÓRIE',
	play = 'HRAJ',

	-- select level
	select_level_head = 'VOĽBA ÚROVNE',

	-- game
	pause = 'PAUZA',
	level = 'ÚROVEŇ',
	numbers = 'ČÍSLA',
	paused = 'PAUZA',
	continue_play = 'POKRAČUJ',
	main_menu = 'HLAVNÉ MENU',
	solved = 'Paráda',
	good_job = 'Hádanka vyriešená!',
	score = 'SCORE',
	play_next_level = 'ĎALŠIA HRA',
	replay = 'OPAKUJ',
	time_out = 'ČAS VYPRŠAL',
}

return values