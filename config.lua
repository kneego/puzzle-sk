application = {
	content = {
		width = 320,
		height = 480, 
		scale = "letterBox",
		fps = 30,
		
        imageSuffix = {
		    ["@2x"] = 1.42857143,
		}
	},

    --[[
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]    
}

local _ar = display.pixelHeight / display.pixelWidth
if _ar > 1.4 then
    application['content']['height'] = 320 * _ar
else
    -- ipad
    application['content']['width'] = 360
    application['content']['height'] = 360 * _ar
end